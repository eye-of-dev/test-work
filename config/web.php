<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'mainpage/default/index',
    'modules' => [
        'books' => [
            'class' => 'app\modules\books\RModule',
        ],
        'mainpage' => [
            'class' => 'app\modules\mainpage\RModule',
        ],
        'users' => [
            'class' => 'app\modules\users\RModule',
        ],
    ],
    'components' => [
        'request' => [
            'enableCsrfValidation' => true,
            'cookieValidationKey' => 'OE1jH5tjixwbDoxujJDR3cSUUimukbSA',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\users\models\UserIdentity',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '',
            'rules' => [
                '/' => 'mainpage/default/index',
                '<module>/<action>/<id:\d+>' => '<module>/default/<action>',
                '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'mainpage/default/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource'
                ],
            ],
        ],
    ],
    'params' => $params,
];

return $config;
