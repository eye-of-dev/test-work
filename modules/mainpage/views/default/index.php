<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\books\models\BooksSearch */

use app\modules\authors\models\Authors;

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

use himiklab\colorbox\Colorbox;

$this->title = 'Main Page';
?>
<?= Colorbox::widget([
    'targets' => [
        '.colorbox' => [
            'maxHeight' => 800
        ],
        '.ajax' => [
            'maxWidth' => 800
        ]
    ],
    'coreStyle' => 5
]) ?>
<div class="books-index">
    <div class="widget">
        <?php $form = ActiveForm::begin(['method' => 'get']); ?>
        <div class="widget-header">
            <h2 class="widget-toggle">Фильтры:</h2>
        </div>
        <div class="widget-content padding">
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'author_id')->dropDownList(Authors::getAuthors(), ['prompt' => \Yii::t('books', 'Выберите автора')]); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'name'); ?>
                </div>
            </div>
            <label class="control-label">Дата выхода книги:</label>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'date_start')->widget(DatePicker::className(), [
                        'options' => ['class' => 'form-control'],
                        'dateFormat' => 'php:d.m.Y'
                    ]); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'date_end')->widget(DatePicker::className(), [
                        'options' => ['class' => 'form-control'],
                        'dateFormat' => 'php:d.m.Y'
                    ]); ?>
                </div>
                <div class="col-md-1">
                    <label class="control-label">&nbsp;</label>
                    <?php echo Html::submitButton('Найти', ['class' => 'btn btn-info form-control', 'id' => 'search']); ?>
                </div>
                <div class="col-md-1">
                    <label class="control-label">&nbsp;</label>
                    <?php echo Html::a('Сброс', Url::to(['/']), ['class' => 'btn btn-info form-control']); ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <p><?= Html::a(Yii::t('books', '<i class="icon-list-add"></i> Добавить книгу'), Url::to(['/books/default/create']), ['class' => 'btn btn-success', 'target' => '_blank']) ?></p>
    <div class="widget">
        <div class="widget-content">
            <div class="table-responsive">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}\n{pager}",
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'name',
                        [
                            'attribute' => 'preview',
                            'format' => 'raw',
                            'value' => function ($model){
                                if($model->preview){
                                    return Html::a(Html::img($model->getFilePath('preview'), ['width' => '100']), $model->getFilePath('preview'), ['class' => 'colorbox']);
                                }
                            },
                        ],
                        [
                            'attribute' => 'author_id',
                            'value' => function ($model){
                                return $model->author->getAuthorFio();
                            },
                        ],
                        [
                            'attribute' => 'date',
                            'value' => function ($model){
                                return date('d.m.Y', strtotime($model->date));
                            },
                        ],
                        [
                            'attribute' => 'create_date',
                            'value' => function ($model){
                                return date('d.m.Y H:i', strtotime($model->create_date));
                            },
                        ],
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'buttons'=>[
                                'view'=> function ($url, $model) {
                                    $link = Url::to(['/books/default/view', 'id' => $model->id]);
                                    return Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $link, ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0', 'class' => 'ajax btn btn-info']);
                                },
                                'update'=> function ($url, $model) {
                                    $link = Url::to(['/books/default/update', 'id' => $model->id]);
                                    return Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $link, ['title' => Yii::t('yii', 'Update'), 'data-pjax' => '0', 'class' => 'btn btn-success', 'target' => '_blank']);
                                },
                                'delete'=> function ($url, $model) {
                                    $link = Url::to(['/books/default/delete', 'id' => $model->id]);
                                    return Html::a( '<span class="glyphicon glyphicon-trash"></span>', $link, ['title' => Yii::t('yii', 'Delete'), 'data-pjax' => '0', 'data-confirm' => \Yii::t('yii', 'Are you sure you want to delete this item?'), 'class' => 'btn btn-danger']);                                        
                                }
                             ],
                            'template'=>'{view} {update} {delete}',
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>