<?php

namespace app\modules\mainpage\controllers;

use app\modules\books\models\BooksSearch;
use Yii;
use yii\web\Controller;

class DefaultController extends Controller
{

    private $index = 'index';

    public function actionIndex()
    {
        $searchModel = new BooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->user->isGuest) {
            $this->index = 'index-guest';
        }

        return $this->render($this->index, [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionError()
    {
        return $this->render('error');
    }

}
