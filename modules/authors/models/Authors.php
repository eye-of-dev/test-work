<?php

namespace app\modules\authors\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "authors".
 */
class Authors extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['first_name', 'last_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('authors', 'ID'),
            'first_name' => Yii::t('authors', 'Имя'),
            'last_name' => Yii::t('authors', 'Фамилия')
        ];
    }

    /**
     * 
     * @return string
     */
    public function getAuthorFio()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * 
     * @return array
     */
    public static function getAuthors()
    {
        $authors = [];

        $results = self::find()->all();
        if ($results) {
            foreach ($results as $author) {
                $authors[$author->id] = $author->first_name . ' ' . $author->last_name;
            }
        }

        return $authors;
    }

}
