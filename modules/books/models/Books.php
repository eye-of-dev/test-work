<?php

namespace app\modules\books\models;

use app\modules\authors\models\Authors;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\UploadedFile;

/**
 * This is the model class for table "books".
 */
class Books extends ActiveRecord
{

    /**
     *
     * @var string 
     */
    private $_doc;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'author_id'], 'required'],
            [['author_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['date'], 'safe'],
            [['preview'], 'file', 'extensions' => 'gif, jpg, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('books', 'ID'),
            'name' => Yii::t('books', 'Название'),
            'preview' => Yii::t('books', 'Обложка'),
            'date' => Yii::t('books', 'Дата издания'),
            'author_id' => Yii::t('books', 'Автор'),
            'create_date' => Yii::t('books', 'Дата создания'),
        ];
    }

    public function behaviors()
    {
        return [
            'Timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }

    /**
     * 
     */
    public function afterFind()
    {
        $this->_doc = $this->preview;
    }

    /**
     * 
     * @return type
     */
    public function beforeValidate()
    {

        if (($file = UploadedFile::getInstance($this, 'preview')) && $file->error === 0) {
            $this->preview = self::saveUploaded($file, self::tableName());
        }

        return parent::beforeValidate();
    }

    /**
     * 
     * @param object $insert
     * @return type
     */
    public function beforeSave($insert)
    {
        if (empty($this->preview)) {
            $this->preview = $this->_doc;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param string $attribute
     * @param bool $abs
     * @param string $mode
     * @return string filename or false if there's no image
     */
    public function getFilePath($attribute, $abs = false, $mode = null)
    {
        if (!$this->$attribute) {
            return false;
        }

        $pathinfo = pathinfo($this->$attribute);
        return "/" . Yii::getAlias('media') . "/" . self::tableName() . "/" . $pathinfo['dirname'] . "/" . $pathinfo['filename'] . $mode . "." . $pathinfo['extension'];
    }

    /**
     * @param UploadedFile $file
     * @param string $path
     * @param bool $subdir
     * @return string new name
     */
    public static function saveUploaded($file, $path, $subdir = true)
    {
        $path = static::getStoragePath(true, $path);
        if ($subdir) {
            $subdir = static::generateSubdir($path);
        }
        else {
            $subdir = '';
            if (!is_dir($path)) {
                if (!is_dir(dirname($path))) {
                    mkdir(dirname($path));
                }
                mkdir($path);
            }
        }
        $newName = static::generateName($file->extension);
        $result = $subdir ? $subdir . '/' . $newName : $newName;
        $saved = $file->saveAs($path . '/' . $result);
        if (!$saved) {
            throw new \Exception('Could not save uploaded file: ' . $file->error . ' to path ' . $path . '/' . $result);
        }
        return $result;
    }

    public static function getStoragePath($abs = false, $name = null)
    {
        $path = \Yii::getAlias('media');

        if (!$name) {
            return $path;
        }
        else {
            return $path . '/' . trim($name, '/');
        }
    }

    public static function generateSubdir($path, $check = true)
    {
        if ($check && !is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $subdir = time();
        //images with "ad" in path get blocked by adblocker
        if ($subdir == "ad") {
            $subdir = "/da";
        }
        if ($path && $check && !is_dir($path . '/' . $subdir)) {
            mkdir($path . '/' . $subdir);
        }
        return $subdir;
    }

    public static function generateName($ext, $suffix = null)
    {
        if (!$suffix) {
            return time() . '.' . $ext;
        }
        return time() . '-' . $suffix . '.' . $ext;
    }

}
