<?php

namespace app\modules\books\models;

use app\modules\books\models\Books;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * BooksSearch represents the model behind the search form about `app\modules\books\models\Books`.
 */
class BooksSearch extends Books
{

    public $date_start;
    public $date_end;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id'], 'integer'],
            [['name', 'date_start', 'date_end'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
                    'date_start' => 'Начало периода',
                    'date_end' => 'Конец периода'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Books::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'author_id' => $this->author_id,
        ]);

        if (!empty($this->date_start)) {
            $query->andWhere('DATE_FORMAT(`date`, "%Y-%m-%d") >= :date_start', [':date_start' => date('Y-m-d', strtotime($this->date_start))]);
        }

        if (!empty($this->date_end)) {
            $query->andWhere('DATE_FORMAT(`date`, "%Y-%m-%d") <= :date_end', [':date_end' => date('Y-m-d', strtotime($this->date_end))]);
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

}
