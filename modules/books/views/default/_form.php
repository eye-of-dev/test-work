<?php

use app\modules\authors\models\Authors;

use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

use himiklab\colorbox\Colorbox;

echo Colorbox::widget([
    'targets' => [
        '.colorbox' => [
            'maxHeight' => 800
        ]
    ],
    'coreStyle' => 5
]);

/* @var $this yii\web\View */
/* @var $model app\modules\books\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">
    <div class="widget">
        <div class="widget-content padding">
            <?php $form = ActiveForm::begin([
                    'enableClientValidation' => ($model->isNewRecord) ? true : false,
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => 255]); ?>

            <?= $form->field($model, 'preview')->fileInput(); ?>

            <?php if($model->preview): ?>
            <div class="form-group">
                <p><?= Html::a(Html::img($model->getFilePath('preview'), ['width' => '100']), $model->getFilePath('preview'), ['class' => 'colorbox']); ?></p>
                <div class="help-block">
                    <p class="text-info">
                        <em>Загрузка нового отменяет старый</em>
                    </p>
                </div>
            </div>
            <?php endif; ?>
            
            <?= $form->field($model, 'date')->widget(DatePicker::className(), [
                'options' => ['class' => 'form-control'],
                'dateFormat' => 'php:d.m.Y'
            ]); ?>
            
            <?= $form->field($model, 'author_id')->dropDownList(Authors::getAuthors(), ['prompt' => \Yii::t('books', 'Выберите автора')]); ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('books', 'Создать') : Yii::t('books', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
