<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\books\models\Books */

$this->title = Yii::t('books', 'Создание книги');
?>
<div class="books-create">
    <div class="page-heading">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
