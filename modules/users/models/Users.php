<?php

namespace app\modules\users\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "users".
 */
class Users extends ActiveRecord
{

    const ROLE_GUEST = 'guest';
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';

    public static $roles = array(
        self::ROLE_USER => 'Пользователь',
        self::ROLE_ADMIN => 'Администратор'
    );

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password', 'role'], 'required'],
            [['username'], 'unique'],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['first_name', 'last_name'], 'safe'],
            [['is_active'], 'integer'],
            [['username', 'password', 'email'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('users', 'ID'),
            'username' => Yii::t('users', 'Имя пользователя'),
            'password' => Yii::t('users', 'Пароль'),
            'email' => Yii::t('users', 'E-mail'),
            'create_date' => Yii::t('users', 'Дата создания'),
            'update_date' => Yii::t('users', 'Дата изменения'),
            'is_active' => Yii::t('users', 'Активность'),
            'role' => Yii::t('users', 'Роль'),
            'first_name' => Yii::t('users', 'Имя'),
            'last_name' => Yii::t('users', 'Фамилия'),
        ];
    }

    public function behaviors()
    {
        return [
            'Timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

}
