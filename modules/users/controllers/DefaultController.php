<?php

namespace app\modules\users\controllers;

use yii\web\Controller;
use app\modules\users\models\LoginForm;

class DefaultController extends Controller
{

    public function actionLogin()
    {
        if (!\Yii::$app->getUser()->getIsGuest()) {
            $this->goHome();
        }

        $model = new LoginForm;

        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        }

        return $this->render('login', ['model' => $model]);
    }

    public function actionLogout()
    {
        \Yii::$app->getUser()->logout();

        return $this->goHome();
    }

}
