<?php

use yii\db\Migration;
use yii\db\Schema;

class m151120_162857_initMigration extends Migration
{

    private $authors = [
        [
            'first_name' => 'Марк',
            'last_name' => 'Твен'
        ],
        [
            'first_name' => 'Джон',
            'last_name' => 'Толкин'
        ],
        [
            'first_name' => 'Джордж',
            'last_name' => 'Мартин'
        ],
        [
            'first_name' => 'Данте',
            'last_name' => 'Алигьери'
        ]
    ];
    
    private $books = [
        [
            'name' => 'Приключения Гекльберри Финна',
            'preview' => '',
            'date' => '1884-01-01',
            'author_id' => '1'
        ],
        [
            'name' => 'Беовульф',
            'preview' => '',
            'date' => '2014-01-01',
            'author_id' => '2'
        ],
        [
            'name' => 'Путешествия Тафа',
            'preview' => '',
            'date' => '1986-01-01',
            'author_id' => '3'
        ],
        [
            'name' => 'Божественная комедия',
            'preview' => '',
            'date' => '1306-01-01',
            'author_id' => '4'
        ]
    ];

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        //Users table
        $this->createTable('users', [
            'id' => 'pk',
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'password' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'first_name' => Schema::TYPE_STRING,
            'last_name' => Schema::TYPE_STRING,
            'create_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
            'update_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
            'is_active' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'role' => Schema::TYPE_STRING . ' DEFAULT "user"',
                ], $tableOptions);

        $this->createIndex('is_active_users', 'users', 'is_active');

        echo 'DEFAULT USERNAME: admin';
        echo 'DEFAULT PASSWORD: 123456';

        $this->insert('users', [
            'username' => 'admin',
            'password' => '$2y$13$.TW4UJ.e8cTdTusrMH4hd.HAtuTxAxfd04njOsemp5JY5ZG/IvhXm',
            'email' => 'admin@sitename.com',
            'is_active' => 1,
            'role' => 'admin'
        ]);

        //Authors table
        $this->createTable('authors', [
            'id' => 'pk',
            'first_name' => Schema::TYPE_STRING,
            'last_name' => Schema::TYPE_STRING,
                ], $tableOptions);

        foreach ($this->authors as $author) {
            $this->insert('authors', [
                'first_name' => $author['first_name'],
                'last_name' => $author['last_name'],
            ]);
        }

        //Books table
        $this->createTable('books', [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'create_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
            'update_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
            'preview' => Schema::TYPE_STRING . ' NULL DEFAULT NULL',
            'date' => Schema::TYPE_DATE . ' DEFAULT "0000-00-00"',
            'author_id' => Schema::TYPE_INTEGER,
                ], $tableOptions);

        $this->createIndex('author_id_users', 'books', 'author_id');
        $this->addForeignKey('FK_books_authors_author_id', 'books', 'author_id', 'authors', 'id');
        
        foreach ($this->books as $book) {
            $this->insert('books', [
                'name' => $book['name'],
                'create_date' => date('Y-m-d H:i:s'),
                'update_date' => date('Y-m-d H:i:s'),
                'preview' => $book['preview'],
                'date' => $book['date'],
                'author_id' => $book['author_id'],
            ]);
        }
    }

    public function down()
    {
        $this->dropTable('books');
        $this->dropTable('authors');
        $this->dropTable('users');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
