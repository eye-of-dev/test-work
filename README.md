# README #

REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.4.0.


#### COMPOSER

**on root web app**

```html
php composer.phar global require "fxp/composer-asset-plugin:1.0.0-beta4" or newer
run: php composer.phar install or php composer.phar update
```
[More information](https://getcomposer.org/doc/00-intro.md)

### Database

Edit the file ` common/config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'YOUR LOGIN',
    'password' => 'YOUR PASSWORD',
    'charset' => 'utf8',
];
```

#### Run migration

```php
yii migrate
```